Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: HTML sanitizer
Upstream-Contact: https://github.com/matthiask/html-sanitizer/issues
Source: https://github.com/matthiask/html-sanitizer

Files: *
Copyright:
  2012-2017  Feinheit AG and individual contributors
License-Grant:
 BSD License
License: BSD-3-Clause~Feinheit
Reference: setup.py
Comment:
 License stated only vaguely in file <setup.py>,
 is presumed to be same as contained in file <LICENSE>,
 and is presumed to apply generally.

Files:
 debian/*
Copyright:
  2020-2021  Jonas Smedegaard <dr@jones.dk>
  2020-2021  Purism, SPC
License-Grant:
 This program is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation;
 either version 3 of the License, or (at your option) any later version.
License: GPL-3+
Reference: debian/copyright

License: BSD-3-Clause~Feinheit
 Redistribution and use in source and binary forms,
 with or without modification,
 are permitted provided that the following conditions are met:
 .
  1. Redistributions of source code must retain
     the above copyright notice, this list of conditions
     and the following disclaimer.
  2. Redistributions in binary form must reproduce
     the above copyright notice, this list of conditions
     and the following disclaimer
     in the documentation and/or other materials
     provided with the distribution.
  3. Neither the name of Feinheit AG
     nor the names of its contributors
     may be used to endorse or promote products
     derived from this software
     without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED
 BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3+
Reference: /usr/share/common-licenses/GPL-3
